# Run k3s as a service in GitLab CI

This project extends [the official k3s images](https://hub.docker.com/r/rancher/k3s) with the following additions for more convenient use in GitLab CI:

- Exposes port `6443` by default
- Serves `/etc/rancher/k3s/k3s.yaml` over port `8081` with an overridable hostname. This should be used as `KUBECONFIG` in the CI job or other services
- The default command is `k3s server` instead of `k3s agent`

## Usage

```yaml
test:
  stage: test
  image: registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image/releases/3.7.1-kube-1.20.11-alpine-3.14
  services:
    - name: registry.gitlab.com/gitlab-org/cluster-integration/test-utils/k3s-gitlab-ci/releases/v1.22.2-k3s2
      alias: k3s
  script:
    - apk add curl
    - curl -f k3s:8081 > k3s.yaml
    - export KUBECONFIG=$(pwd)/k3s.yaml
    - kubectl version
    - kubectl cluster-info
```

The above interface is supported for the following k3s versions:

- `v1.20.15-k3s1`
- `v1.21.10-k3s1`
- `v1.22.7-k3s1`
- `v1.23.4-k3s1`

### Using a test matrix

You set up a test matrix to ensure compatibility across multiple Kubernetes versions:

```yaml
test:
  stage: test
  image: registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image/releases/2.16.6-kube-1.13.12
  parallel:
    matrix:
      - K3S_VERSION:
        - v1.20.15-k3s1
        - v1.21.10-k3s1
        - v1.22.7-k3s1
        - v1.23.4-k3s1

  services:
    - name: registry.gitlab.com/gitlab-org/cluster-integration/test-utils/k3s-gitlab-ci/releases/$K3S_VERSION
      alias: k3s
  script:
    - apk add curl
    - curl k3s:8081 > k3s.yaml
    - export KUBECONFIG=$(pwd)/k3s.yaml
    - kubectl version
    - kubectl cluster-info
```

### Using a  `service:alias` other than `k3s`

To use a service alias other than `k3s`:

1. Append `?service=<YOUR_SERVICE_ALIAS>` to the kubeconfig request. So the script becomes:

   ```yaml
   script:
     - apk add curl
     - curl -f <YOUR_SERVICE_ALIAS>:8081?service=<YOUR_SERVICE_ALIAS> > k3s.yaml
     - export KUBECONFIG=$(pwd)/k3s.yaml
    ```

1. Add your alias as an additional TLS SAN on the Kubernetes API server by overriding the `service:command`:

   ```yaml
   services:
     - name: registry.gitlab.com/gitlab-org/cluster-integration/test-utils/k3s-gitlab-ci/releases/v1.16.7-k3s1
       alias: <YOUR_SERVICE_ALIAS>
       command: [server, "--tls-san=<YOUR_SERVICE_ALIAS>"]
   ```

   If you do not not add the TLS SAN, you may intermittently see the error

   > Unable to connect to the server: x509: certificate is valid for kubernetes, kubernetes.default, kubernetes.default.svc.cluster.local, localhost, not `<YOUR_SERVICE_ALIAS>`
